package com.example.pichurchyk_l6_t1

import android.widget.Button

interface Communicator {
    fun passData(userName: String, userPhone: String, userEmail: String)
}