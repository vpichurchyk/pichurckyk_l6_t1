package com.example.pichurchyk_l6_t1.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.pichurchyk_l6_t1.R

class Fragment_user_data : Fragment(R.layout.fragment_user_data) {

    private var userName: String? = null
    private var userPhone: String? = null
    private var userEmail: String? = null

    private var tvUserName: TextView? = null
    private var tvUserPhone: TextView? = null
    private var tvUserEmail: TextView? = null

    private var btnCall: Button? = null
    private var btnEmail: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user_data, container, false)

        tvUserName = view.findViewById(R.id.user_name)
        tvUserPhone = view.findViewById(R.id.user_phone)
        tvUserEmail = view.findViewById(R.id.user_email)

        userName = arguments?.getString("userName")
        userPhone = arguments?.getString("userPhone")
        userEmail = arguments?.getString("userEmail")

        tvUserName?.text = userName
        tvUserPhone?.text = userPhone
        tvUserEmail?.text = userEmail

        btnCall = view.findViewById(R.id.btn_call)
        btnCall?.setOnClickListener{
            makeCall()
        }

        btnEmail = view.findViewById(R.id.btn_email)
        btnEmail?.setOnClickListener{
            sendEmail()
        }

        return view
    }

    private fun makeCall() {
        if(tvUserPhone?.text.toString() == "") {
            Toast.makeText(context, "You must input your info first", Toast.LENGTH_SHORT).show()
        }
        else {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            val phoneNumber: String = tvUserPhone?.text.toString()
            dialIntent.data = Uri.parse("tel:$phoneNumber")
            startActivity(dialIntent)
        }
    }

    private fun sendEmail() {
        if (tvUserPhone?.text.toString() == "" || tvUserEmail?.text.toString() == "" || tvUserName?.text.toString() == "") {
            Toast.makeText(context, "You must input your info first", Toast.LENGTH_SHORT).show()
        }
        else {
            val mIntent = Intent(Intent.ACTION_SENDTO)
            mIntent.data = Uri.parse("mailto:")
            mIntent.putExtra(Intent.EXTRA_SUBJECT, "My personal info")
            mIntent.putExtra(Intent.EXTRA_TEXT, "$userName \nEmail: $userEmail \nPhone number: $userPhone")
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        }
    }
}