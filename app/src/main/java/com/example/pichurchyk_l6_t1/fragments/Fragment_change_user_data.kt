package com.example.pichurchyk_l6_t1.fragments

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.pichurchyk_l6_t1.Communicator
import com.example.pichurchyk_l6_t1.R
import java.util.regex.Matcher
import java.util.regex.Pattern

class Fragment_change_user_data() : Fragment(R.layout.fragment_change_user_data) {

    private lateinit var communicator: Communicator

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_change_user_data, container, false)
        communicator = activity as Communicator

        val btnSave = activity?.findViewById<Button>(R.id.btn_save)
        btnSave?.setOnClickListener{
            val userName: String = view.findViewById<EditText>(R.id.edit_user_name).text.toString()
            val userPhone: String = view.findViewById<EditText>(R.id.edit_user_phone).text.toString()
            val userEmail: String = view.findViewById<EditText>(R.id.edit_user_email).text.toString()

            if (userName == "" ||  !emailValidator(userEmail)) {
                Toast.makeText(context, "You must input all info", Toast.LENGTH_SHORT).show()
            }
            else {
                communicator.passData(userName, userPhone, userEmail)
            }
       }
        return view
    }

    private fun emailValidator(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}