package com.example.pichurchyk_l6_t1

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Camera
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.speech.RecognizerIntent
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.example.pichurchyk_l6_t1.fragments.Fragment_change_user_data
import com.example.pichurchyk_l6_t1.fragments.Fragment_user_data
import java.util.*

var imgBitmap: Bitmap? = null

class MainActivity : AppCompatActivity(), Communicator {
    private var toolbar: Toolbar? = null
    private var imgView: ImageView? = null
    private val rqPickImage = 100

    var isChanging: Boolean = false

    private var tvUserName: TextView? = null
    private var tvUserPhone: TextView? = null
    private var tvUserEmail: TextView? = null

    private var btnEdit: Button? = null
    private var btnSave: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showData()

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(0)

        btnSave = findViewById(R.id.btn_save)
        btnEdit = findViewById(R.id.btn_edit)
        btnEdit?.setOnClickListener{
            if (!isChanging){
                editData()
                btnSave?.visibility = View.VISIBLE
            }
        }

        imgView = findViewById(R.id.user_img)
        imgView?.setOnClickListener{
            if (isChanging) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), rqPickImage)
                }
                else {
                    takePhoto()
                }
            }
        }
    }
    private fun showData(userName: String = "", userPhone: String = "", userEmail: String = ""){
        val bundle = Bundle()
        bundle.putString("userName", userName)
        bundle.putString("userPhone", userPhone)
        bundle.putString("userEmail", userEmail)
        val userData = Fragment_user_data()
        userData.arguments = bundle
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragments_container, userData)
            commit()
        }
        isChanging = false
        btnSave?.visibility = View.GONE
        btnEdit?.visibility = View.VISIBLE
    }

    private fun editData(){
        val userData = Fragment_change_user_data()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragments_container, userData)
            commit()
        }
        isChanging = true
        btnEdit?.visibility = View.GONE
    }

    private fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, rqPickImage)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == rqPickImage && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            takePhoto()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == rqPickImage){
            imgBitmap = data?.getParcelableExtra<Bitmap>("data")
            if (imgBitmap != null) {
                imgView?.setImageBitmap(imgBitmap)
            }
        }
    }

    override fun passData(userName: String, userPhone: String, userEmail: String) {
        showData(userName, userPhone, userEmail)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        tvUserName = findViewById(R.id.user_name)
        tvUserPhone = findViewById(R.id.user_phone)
        tvUserEmail = findViewById(R.id.user_email)

        outState.putString("name", tvUserName?.text.toString())
        outState.putString("phone", tvUserPhone?.text.toString())
        outState.putString("email", tvUserEmail?.text.toString())
        outState.putParcelable("img", imgBitmap)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        tvUserName = findViewById(R.id.user_name)
        tvUserPhone = findViewById(R.id.user_phone)
        tvUserEmail = findViewById(R.id.user_email)
        imgView = findViewById(R.id.user_img)

        tvUserName?.text = savedInstanceState.getString("name")
        tvUserPhone?.text = savedInstanceState.getString("phone")
        tvUserEmail?.text = savedInstanceState.getString("email")
        if(savedInstanceState.getParcelable<Bitmap>("img") != null) {
            imgView?.setImageBitmap(savedInstanceState.getParcelable("img"))
        }
    }
}